### Summary ###
This repo contains about C api for alljoyn framework.

### How do I get set up? ###
cd $AJ_ROOT/alljoyn_c/samples
mkdir about
Copy SConscript from the root of repo into this dir.
Place everything from Samples dir into about dir you've created.
cd $AJ_ROOT/alljoyn_c/unit_test
Place everything from Tests dir of repo into this directory($AJ_ROOT/alljoyn_c/unit_test)

### Usage ###
cd to usual directory where ajctest is placed.
Run all about tests.
./ajctest --gtest_filter=About*
